<?php


namespace Front\Model;


use Core\Database\Model;

class Keyword extends Model
{
    /**
     * @type varchar
     * @size 1024
     * @var $title
     */
    public $title;
}